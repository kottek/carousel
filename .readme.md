# ReactCarousel

Clone and npm install

Usage:

```
const Slide = (props) => <img {...props} alt="slide" />

const App = () => <ReactCarousel>
    <Slide src="#" />
    <Slide src="#" />
    <Slide src="#" />
</ReactCarousel>
```

## Options

Component comes with some default settings, that can be adjusted via props.

```
resizeDebounce: 250
duration: 200
easing: 'ease-out'
perPage: 1
startIndex: 0
draggable: true
threshold: 20
loop: false
```

Example of passing custom options:

```
const Slide = (props) => <img {...props} alt="slide" />

const options = {
    duration: 500,
    loop: true
}

const App = () => <ReactCarousel {...options}>
    <Slide src="#" />
    <Slide src="#" />
    <Slide src="#" />
</ReactCarousel>
```

## API

- `next()` - go to next slide
- `prev()` - go to previous slide
- `goTo(index)` - go to a specific slide
- `currentSlide` - index of the current active slide (read only)

## Example of API usage

API is accessible via refs.

```
const Slide = (props) => <img {...props} alt="slide" />

const App = () => {
    let slider

    return (
        <div>
            <ReactCarousel ref={Carousel => slider = Carousel}>
                <Slide src="#" />
                <Slide src="#" />
                <Slide src="#" />
            </ReactCarousel>
            <button onClick={() => slider.prev()}>prev</button>
            <button onClick={() => slider.next()}>next</button>
        </div>
    )
}
```

## Click Events

This version of react-Carousel has been extended to include the ability to attach very simple click events to items passed into the slider. It will only fire the click event if the slide wasn't dragged.

Provide a normal event handler method the `onClick` prop in `<ReactCarousel>`. Current slide information can be retrieved from the instance of Carousel. (`this.slider.currentSlide` below).

```
const Slide = (props) => <img {...props} alt="slide" />

class App extends Component {
    constructor() {
        this.slider = null;
    }

    handleClick = (e) => {
        console.log(`Index of the clicked slide is ${this.slider.currentSlide}`);
    }

    render() {
        return (
            <div>
                <ReactCarousel ref={Carousel => this.slider = Carousel} onClick={this.handleClick}>
                    <Slide src="#" />
                    <Slide src="#" />
                    <Slide src="#" />
                </ReactCarousel>
                <button onClick={() => slider.prev()}>prev</button>
                <button onClick={() => slider.next()}>next</button>
            </div>
        )
    }
}
```
